<?php
    namespace jaapvanwingerden\imagetocss;

    class simpleImageProcessor {
        protected $image = false;

		public function loadImage($filename){
			if (!file_exists($filename)){
				throw new \Exception('Failed to load file '. $filename);
			}
			$this->image = imagecreatefrompng($filename);
			imagepalettetotruecolor($this->image); // Now we can always assume true color.
		}

        public function process(){
			if (!$this->image)
                throw new \Exception('Cannot process image while no image has been loaded.');

            $colors = array();
            $colorCount = array();
            for ($x = 0; $x < imagesx($this->image); $x++){
                for ($y = 0; $y < imagesy($this->image); $y++){
                    $colors[] = $this->colorToCss(imagecolorat($this->image, $x, $y));
                }
            }
            $colors = array_unique($colors);
            $colorCount = array();
            for ($x = 0; $x < imagesx($this->image); $x++){
                for ($y = 0; $y < imagesy($this->image); $y++){
                    $key = array_search($this->colorToCss(imagecolorat($this->image, $x, $y)), $colors);
                    if (!array_key_exists($key, $colorCount)){
                        $colorCount[$key] = 0;
                    }
                    $colorCount[$key]++;
                }
            }
            $bestKey = false;
            $bestCount = 0;
            foreach ($colorCount as $key => $col){
                if ($col > $bestCount){
                    $bestCount = $col;
                    $bestKey   = $key;
                }
            }
            printf('<html><head><title>Testbestand afbeelding naar html</title><style>');
            printf('table, tr, td { border-style: none; margin: 0px; padding: 0px; border-collapse: collapse; background-color: %s;} tr { width: %dpx; height: 1px;} td {width: 1px; height: 1px; } ', $colors[$bestKey], imagesy($this->image));
            printf('#imgtable {width: %dpx; height: %dpx;}', imagesx($this->image), imagesy($this->image));
            foreach($colors as $key => $color){
                printf(' .c%d {background-color: %s;}', $key, $color);
            }
            printf('</style></head><body><table id="imgtable">');
            for ($y = 0; $y < imagesy($this->image); $y++){
                printf('<tr>');
                for ($x = 0; $x < imagesx($this->image); $x++){
                    $cscount = 0;
                    while (
                        (($x + $cscount + 1) < imagesx($this->image)) &&
                        (imagecolorat($this->image, $x, $y) == imagecolorat($this->image, $x + $cscount + 1, $y))
                    ){
                        $cscount++;
                    }
                    $colspan = (0 == $cscount) ? '' : sprintf(' colspan="%d"', $cscount + 1);
                    $cClass = '';
                    $currentKey = array_search($this->colorToCss(imagecolorat($this->image, $x, $y)), $colors);
                    if (!($currentKey == $bestKey)){
                        $cClass = sprintf(' class="c%s"', $currentKey);
                    }
                    printf('<td%s%s />', $cClass, $colspan);
                    $x += $cscount;
                }
                printf('</tr>');
            }
            printf('</table></body></html>');
        }

		static function colorToCss($color){
            /*
			$result['red'] = ($color >> 16) & 0xFF;
			$result['green'] = ($color >> 8) & 0xFF;
			$result['blue'] = $color & 0xFF;
			
			$colorcode = 	'#'. 
							str_pad(dechex($result['red']), 2, '0', STR_PAD_LEFT). 
							str_pad(dechex($result['green']), 2, '0', STR_PAD_LEFT). 
                            str_pad(dechex($result['red']), 2, '0', STR_PAD_LEFT);
            printf('Kleurgetal %d (%x) omgezet in R%x, G%x, B%x, wat kleurcode %');
            return $colorcode;
            */
            return '#'. str_pad(sprintf('%x', $color), 6, '0', STR_PAD_LEFT);
		}       
    }

    $ip = new simpleImageProcessor();
    $ip->loadImage('2003107400.png');
    $ip->process();